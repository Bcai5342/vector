package test.Vector;

import static org.junit.Assert.*;

import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;
import org.junit.Test;
import main.Vector;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

/**
 * @author Brenden Cai
 * @email caibrenden@yahoo.com.au
 *
 */
public class Vector_Value_Validation_Tests {

	/**
	 * Test method for {@link main.Vector#isAbundant(long)}.
	 */
	@Test
	public final void isAbundant_LargePositive_True() {
		long number = 5391411025L;
	
		boolean isAbundant = Vector.isAbundant(number);
	
		assertEquals(true, isAbundant);
	}

	/**
	 * Test method for {@link main.Vector#isAbundant(long)}.
	 */
	@Test
	public final void isAbundant_SmallNumberEven_True() {
		long number = 16;
	
		boolean isAbundant = Vector.isAbundant(number);
	
		assertEquals(false, isAbundant);
	}

	/**
	 * Test method for {@link main.Vector#isAbundant(long)}.
	 */
	@Test
	public final void isAbundant_SmallNumberOdd_True() {
		long number = 945;
	
		boolean isAbundant = Vector.isAbundant(number);
	
		assertEquals(true, isAbundant);
	}

	/**
	 * Test method for {@link main.Vector#isAbundant(long)}.
	 */
	@Test
	public final void isAbundant_SmallPositiveLimit_True() {
		long number = 12;
	
		boolean isAbundant = Vector.isAbundant(number);
	
		assertEquals(true, isAbundant);
	}

	/**
	 * Test method for {@link main.Vector#isComposite(long)}.
	 */
	@Test
	public final void isComposite_BigPositiveNumber_False() {
		long compositeNumber = 982451653L;
		boolean isComposite = Vector.isComposite(compositeNumber);

		assertEquals(false, isComposite);
	}

	/**
	 * Test method for {@link main.Vector#isComposite(long)}.
	 */
	@Test
	public final void isComposite_BigPositiveNumber_True() {
		long compositeNumber = 54643216542L;
		boolean isComposite = Vector.isComposite(compositeNumber);

		assertEquals(true, isComposite);
	}

	/**
	 * Test method for {@link main.Vector#isComposite(long)}.
	 */
	@Test
	public final void isComposite_SmallNegativeNumber_False() {
		long compositeNumber = -6;
		boolean isComposite = Vector.isComposite(compositeNumber);

		assertEquals(false, isComposite);
	}

	/**
	 * Test method for {@link main.Vector#isComposite(long)}.
	 */
	@Test
	public final void isComposite_SmallPositive_True() {
		long compositeNumber = 4;
		boolean isComposite = Vector.isComposite(compositeNumber);

		assertEquals(true, isComposite);
	}

	/**
	 * Test method for {@link main.Vector#isComposite(long)}.
	 */
	@Test
	public final void isComposite_SmallPositiveNumber_False() {
		long compositeNumber = 3;
		boolean isComposite = Vector.isComposite(compositeNumber);

		assertEquals(false, isComposite);
	}

	/**
	 * Test method for {@link main.Vector#isPQ(long)}.
	 */
	@Test
	public final void isPQ_LargePositive_False() {
		long semiPrime = 44132588L; // (7919 * 5573) +1
		boolean isPQ = Vector.isPQ(semiPrime);

		assertEquals(false, isPQ);
	}

	/**
	 * Test method for {@link main.Vector#isPQ(long)}.
	 */
	@Test
	public final void isPQ_LargePositive_True() {
		long semiPrime = 44132587L; // 7919 * 5573
		boolean isPQ = Vector.isPQ(semiPrime);

		assertEquals(true, isPQ);
	}

	/**
	 * Test method for {@link main.Vector#isPQ(long)}.
	 */
	@Test
	public final void isPQ_SmallPositive_False() {
		long semiPrime = 5L;
		boolean isPQ = Vector.isPQ(semiPrime);

		assertEquals(false, isPQ);
	}

	/**
	 * Test method for {@link main.Vector#isPQ(long)}.
	 */
	@Test
	public final void isPQ_SmallPositive_True() {
		long semiPrime = 4L;
		boolean isPQ = Vector.isPQ(semiPrime);

		assertEquals(true, isPQ);
	}

	/**
	 * Test method for {@link main.Vector#isPrime(long)}.
	 */
	@Test
	public final void isPrime_LargePositive_False() {
		long primeNumber = 40000000001L;
		boolean isPrime = Vector.isPrime(primeNumber);

		assertEquals(false, isPrime);
	}

	/**
	 * Test method for {@link main.Vector#isPrime(long)}.
	 */
	@Test
	public final void isPrime_LargePositive_True() {
		long primeNumber = 982451653;
		boolean isPrime = Vector.isPrime(primeNumber);

		assertEquals(true, isPrime);
	}

	/**
	 * Test method for {@link main.Vector#isPrime(long)}.
	 */
	@Test
	public final void isPrime_LongMaxPositive_True() {
		long primeNumber = Long.MAX_VALUE;
		boolean isPrime = Vector.isPrime(primeNumber);

		assertEquals(false, isPrime);
	}

	/**
	 * Test method for {@link main.Vector#isPrime(long)}.
	 */
	@Test
	public final void isPrime_SmallNegative_False() {
		long primeNumber = -3;
		boolean isPrime = Vector.isPrime(primeNumber);

		assertEquals(false, isPrime);
	}

	/**
	 * Test method for {@link main.Vector#isPrime(long)}.
	 */
	@Test
	public final void isPrime_SmallPositive_False() {
		long primeNumber = 1097;
		boolean isPrime = Vector.isPrime(primeNumber);

		assertEquals(true, isPrime);
	}

	/**
	 * Test method for {@link main.Vector#isPrime(long)}.
	 */
	@Test
	public final void isPrime_SmallPositive_True() {
		long primeNumber = 3;
		boolean isPrime = Vector.isPrime(primeNumber);

		assertEquals(true, isPrime);
	}

}
