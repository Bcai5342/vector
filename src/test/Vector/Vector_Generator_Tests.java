package test.Vector;

import static org.junit.Assert.*;

import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import main.Vector;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
/**
 * @author Brenden Cai
 * @email caibrenden@yahoo.com.au
 *
 */
public class Vector_Generator_Tests {

	/**
	 * Test method for {@link main.Vector#abundant(int, long)}.
	 */
	@Test
	public final void testAbundant_NormalLength() {
		Vector v = Vector.abundant(4, 0);

		assertArrayEquals(new long[] { 12, 18, 20, 24 }, v.getElements());
	}

	/**
	 * Test method for {@link main.Vector#cloned()}.
	 */
	@Test
	public final void testCloned() {
		Vector v1 = Vector.random(10, 0);
		Vector v2 = v1.cloned();

		assertArrayEquals(v2.getElements(), v1.getElements());
	}

	/**
	 * Test method for {@link main.Vector#pq(int, long)}.
	 */
	@Test
	public final void testPq() {
		Vector v = Vector.pq(34, 0);

		long[] testArray = new long[] { 4, 6, 9, 10, 14, 15, 21, 22, 25, 26, 33, 34, 35, 38, 39, 46, 49, 51, 55, 57, 58,
				62, 65, 69, 74, 77, 82, 85, 86, 87, 91, 93, 94, 95 };

		assertArrayEquals(testArray, v.getElements());
	}

	/**
	 * Test method for {@link main.Vector#pq(int, long)}.
	 */
	@Test
	@Ignore
	public final void testPq_Large() {
		Vector v = Vector.pq(1000, 0);

		long[] testArray = new long[] { 4, 6, 9, 10, 14, 15, 21, 22, 25, 26, 33, 34, 35, 38, 39, 46, 49, 51, 55, 57, 58,
				62, 65, 69, 74, 77, 82, 85, 86, 87, 91, 93, 94, 95 };

		fail("Bad Data");
		assertArrayEquals(testArray, v.getElements());
	}

	/**
	 * Test method for {@link main.Vector#prime(int, long)}.
	 */
	@Test
	public final void testPrime() {
		Vector v = Vector.prime(100, 0);

		long[] testArray = new long[] { 2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73,
				79, 83, 89, 97, 101, 103, 107, 109, 113, 127, 131, 137, 139, 149, 151, 157, 163, 167, 173, 179, 181,
				191, 193, 197, 199, 211, 223, 227, 229, 233, 239, 241, 251, 257, 263, 269, 271, 277, 281, 283, 293, 307,
				311, 313, 317, 331, 337, 347, 349, 353, 359, 367, 373, 379, 383, 389, 397, 401, 409, 419, 421, 431, 433,
				439, 443, 449, 457, 461, 463, 467, 479, 487, 491, 499, 503, 509, 521, 523, 541 };

		assertArrayEquals(testArray, v.getElements());

	}

	/**
	 * Test method for {@link main.Vector#random(int, long)}.
	 */
	@Test
	public final void testRandom_Equality_SameLength_SameSeed() {
		Vector v1 = Vector.random(10, 0);
		Vector v2 = Vector.random(10, 0);
		assertArrayEquals(v1.getElements(), v2.getElements());
	}

	/**
	 * Test method for {@link main.Vector#random(int, long)}.
	 */
	@Test
	public final void testRandom_ZeroLength_ZeroSeed() {
		Vector v = Vector.random(0, 0);
		assertArrayEquals(new long[0], v.getElements());
	}

	/**
	 * Test method for {@link main.Vector#reversed()}.
	 */
	@Test
	public final void testReversed_NormalLength() {
		Vector v = Vector.sequence(5, 0, 3);
		Vector v2 = v.reversed();

		assertArrayEquals(new long[] { 12, 9, 6, 3, 0 }, v2.getElements());
	}

	/**
	 * Test method for {@link main.Vector#reversed()}.
	 */
	@Test
	public final void testReversed_ZeroLength() {
		Vector v = Vector.sequence(0, 0, 1);
		Vector v2 = v.reversed();

		assertArrayEquals(new long[0], v2.getElements());
	}

	/**
	 * Test method for {@link main.Vector#scalarAdd(long)}.
	 */
	@Test
	public final void testScalarAdd_NormalLengthNegativeScalar() {
		Vector v = Vector.sequence(10, 1, 1);
		Vector v2 = v.scalarAdd(-1);

		assertArrayEquals(new long[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 }, v2.getElements());

	}

	/**
	 * Test method for {@link main.Vector#scalarAdd(long)}.
	 */
	@Test
	public final void testScalarAdd_NormalLengthPositiveScalar() {
		Vector v = Vector.sequence(10, 1, 1);
		Vector v2 = v.scalarAdd(1);

		assertArrayEquals(new long[] { 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 }, v2.getElements());
	}

	/**
	 * Test method for {@link main.Vector#scalarMultiply(long)}.
	 */
	@Test
	public final void testScalarMultiply_NormalLengthNegativeScalar() {
		Vector v = Vector.sequence(10, 1, 1);
		Vector v2 = v.scalarMultiply(-2);

		assertArrayEquals(new long[] { -2, -4, -6, -8, -10, -12, -14, -16, -18, -20 }, v2.getElements());
	}

	/**
	 * Test method for {@link main.Vector#scalarMultiply(long)}.
	 */
	@Test
	public final void testScalarMultiply_NormalLengthPositiveScalar() {
		Vector v = Vector.sequence(10, 1, 1);
		Vector v2 = v.scalarMultiply(2);

		assertArrayEquals(new long[] { 2, 4, 6, 8, 10, 12, 14, 16, 18, 20 }, v2.getElements());

	}

	/**
	 * Test method for {@link main.Vector#scalarMultiply(long)}.
	 */
	@Test
	public final void testScalarMultiply_NormalLengthZeroScalar() {
		Vector v = Vector.sequence(10, 1, 1);
		Vector v2 = v.scalarMultiply(0);

		assertArrayEquals(new long[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }, v2.getElements());
	}

	/**
	 * Test method for {@link main.Vector#sequence(int, long, long)}.
	 */
	@Test
	public final void testSequence_PositiveLength_NegativeStep() {
		Vector v = Vector.sequence(10, 0, -1);
		long[] testArray = new long[] { 0, -1, -2, -3, -4, -5, -6, -7, -8, -9 };

		assertArrayEquals(testArray, v.getElements());
	}

	/**
	 * Test method for {@link main.Vector#sequence(int, long, long)}.
	 */
	@Test
	public final void testSequence_PositiveLength_PositiveStep() {
		Vector v = Vector.sequence(10, 0, 1);
		long[] testArray = new long[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };

		assertArrayEquals(testArray, v.getElements());

	}

	/**
	 * Test method for {@link main.Vector#sequence(int, long, long)}.
	 */
	@Test
	public final void testSequence_ZeroLength_PositiveStep() {
		Vector v = Vector.sequence(0, 0, 1);
		long[] testArray = new long[0];

		assertArrayEquals(testArray, v.getElements());

	}

	/**
	 * Test method for {@link main.Vector#shifted(int)}.
	 */
	@Test
	public final void testShifted() {
		Vector v = Vector.sequence(5, 1, 1);
		Vector v2 = v.shifted(1);

		assertArrayEquals(new long[] { 5, 1, 2, 3, 4 }, v2.getElements());

	}

	/**
	 * Test method for {@link main.Vector#shifted(int)}.
	 */
	@Test
	public final void testShifted_DoubleShift() {
		Vector v = Vector.sequence(5, 1, 1);
		Vector v2 = v.shifted(2);

		assertArrayEquals(new long[] { 4, 5, 1, 2, 3 }, v2.getElements());

	}

	/**
	 * Test method for {@link main.Vector#shifted(int)}.
	 */
	@Test
	@Ignore
	public final void testShifted_NegativeShift() {
		Vector v = Vector.sequence(5, 1, 1);
		Vector v2 = v.shifted(-3); // Same as shift of 4.

		assertArrayEquals(new long[] { 4, 5, 1, 2, 3 }, v2.getElements());
		fail("TODO");
	}

	/**
	 * Test method for {@link main.Vector#shifted(int)}.
	 */
	@Test
	public final void testShifted_OverShift() {
		Vector v = Vector.sequence(5, 1, 1);
		Vector v2 = v.shifted(11); // Same as shift of 1.

		assertArrayEquals(new long[] { 5, 1, 2, 3, 4 }, v2.getElements());

	}

	/**
	 * Test method for {@link main.Vector#shifted(int)}.
	 */
	@Test
	public final void testShifted_OverShift2() {
		Vector v = Vector.sequence(5, 1, 1);
		Vector v2 = v.shifted(14); // Same as shift of 4.

		assertArrayEquals(new long[] { 2, 3, 4, 5, 1 }, v2.getElements());

	}

	/**
	 * Test method for {@link main.Vector#sorted()}.
	 */
	@Test
	public final void testSorted_LargeLength_() {
		Vector v = Vector.sequence(100000, 0, 1);
		Vector v2 = v.sorted();

		assertArrayEquals(v.getElements(), v2.getElements());
	}

	/**
	 * Test method for {@link main.Vector#sorted()}.
	 */
	@Test
	public final void testSorted_NormalLength_Positive() {
		Vector v = Vector.sequence(5, 5, -1);
		Vector v2 = v.sorted();

		assertArrayEquals(new long[] { 1, 2, 3, 4, 5 }, v2.getElements());
	}

	/**
	 * Test method for {@link main.Vector#sorted()}.
	 */
	@Test
	public final void testSorted_NormalLength_RandomSequence() {
		Vector v = Vector.random(10, 2);
		Vector v2 = v.sorted();

		assertArrayEquals(new long[] { 0, 5, 37, 41, 41, 68, 80, 93, 95, 100 }, v2.getElements());
	}

	/**
	 * Test method for {@link main.Vector#sorted()}.
	 */
	@Test
	public final void testSorted_ZeroLength() {
		Vector v = Vector.sequence(0, 1, 1);
		Vector v2 = v.sorted();

		assertArrayEquals(new long[0], v2.getElements());
	}

	/**
	 * Test method for {@link main.Vector#uniform(int, long)}.
	 */
	@Test
	public final void testUniform_NonZeroLength() {
		Vector v = Vector.uniform(10, 0);
		long[] testArray = new long[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

		assertArrayEquals(testArray, v.getElements());
	}

	/**
	 * Test method for {@link main.Vector#uniform(int, long)}.
	 */
	@Test
	public final void testUniform_ZeroLength() {
		Vector v = Vector.uniform(0, 0);
		assertArrayEquals(new long[0], v.getElements());
	}

	/**
	 * Test method for {@link main.Vector#Vector(int)}.
	 */
	@Test
	public final void testVector_ZeroLength() {
		Vector v = new Vector(0);
		assertEquals(0, v.getLength());
		assertArrayEquals(new long[0], v.getElements());
	}

	/**
	 * Test method for {@link main.Vector#vectorAdd(main.Vector)}.
	 */
	@Test
	public final void testVectorAdd_SameSize_Uniform_FiveMinusFive() {
		Vector v = Vector.uniform(5, 5);
		Vector v2 = Vector.uniform(5, -5);
		Vector v3 = v.vectorAdd(v2);

		assertArrayEquals(new long[] { 0, 0, 0, 0, 0 }, v3.getElements());
	}

	/**
	 * Test method for {@link main.Vector#vectorAdd(main.Vector)}.
	 */
	@Test
	public final void testVectorAdd_SameSize_Uniform_FivePlusFive() {
		Vector v = Vector.uniform(5, 5);
		Vector v2 = Vector.uniform(5, 5);
		Vector v3 = v.vectorAdd(v2);

		assertArrayEquals(new long[] { 10, 10, 10, 10, 10 }, v3.getElements());

	}

	/**
	 * Test method for {@link main.Vector#vectorMultiply(main.Vector)}.
	 */
	@Test
	public final void testVectorMultiply_NegativeScalar() {
		Vector v = Vector.uniform(5, 5);
		Vector v2 = Vector.uniform(5, -5);
		Vector v3 = v.vectorMultiply(v2);

		assertArrayEquals(new long[] { -25, -25, -25, -25, -25 }, v3.getElements());
	}

	/**
	 * Test method for {@link main.Vector#vectorMultiply(main.Vector)}.
	 */
	@Test
	public final void testVectorMultiply_PositiveScalar() {
		Vector v = Vector.uniform(5, 5);
		Vector v2 = Vector.uniform(5, 5);
		Vector v3 = v.vectorMultiply(v2);

		assertArrayEquals(new long[] { 25, 25, 25, 25, 25 }, v3.getElements());

	}
}
