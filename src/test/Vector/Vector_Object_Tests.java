package test.Vector;

import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import main.Vector;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class Vector_Object_Tests {

	private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();

	private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();

	/*-
	 * Array Patterns
	 */
	// Empty Array
	long[] emptyArray = new long[0];
	
	//
	
	/*-
	 * Abundant - abundant
	 * 
	 * BuildMode: ABUNDANT
	 */
	Vector vAbundant00_0 = Vector.abundant(0, 0);
	Vector vAbundant10_0 = Vector.abundant(10, 0);
	Vector vAbundant10_200 = Vector.abundant(10, 200);
	
	/*-
	 * Composite - composite
	 * BuildMode: COMPOSITE
	 */
	Vector vComposite00_0 = Vector.composite(0, 0);
	Vector vComposite10_0 = Vector.composite(10, 0);
	Vector vComposite10_200 = Vector.composite(10, 200);
	
	/*-
	 * None - none
	 * BuildMode: NONE
	 */
	Vector vVector_00 = new Vector(0);
	Vector vVector_10 = new Vector(10);
	Vector vVector_100 = new Vector(100);

	/*
	 * Semiprime - pq 
	 * BuildMode: PQ
	 */
	Vector vPq00_00 = Vector.pq(0, 0);
	Vector vPq10_00 = Vector.pq(10, 0);
	Vector vPq10_200 = Vector.pq(10, 200);

	/*-
	 * Prime - prime 
	 * BuildMode: PRIME
	 */
	Vector vPrime00_0 = Vector.prime(0, 0);
	Vector vPrime10_0 = Vector.prime(10, 0);
	Vector vPrime11_0 = Vector.prime(11, 0);
	Vector vPrime10_200 = Vector.prime(10, 200);

	/*
	 * Random
	 * Known Sequence[10] - 5 37 68 0 41 41 80 100 93 95 
	 * BuildMode: RANDOM
	 */
	Vector vRandom00_2 = Vector.random(0, 2);
	Vector vRandom09_2 = Vector.random(9, 2);
	long[] vRandom09_2_Values = new long[] { 5, 37, 68, 0, 41, 41, 80, 100, 93 };
	Vector vRandom10_2 = Vector.random(10, 2);
	long[] vRandom10_2_Values = new long[] { 5, 37, 68, 0, 41, 41, 80, 100, 93, 95 };
	Vector vRandom1000000_2 = Vector.random(1000000, 2); // 1 Million values

	/*-
	 * Sequence
	 * 0,1,2,3,4,5,6,7,8,9 
	 * BuildMode: SEQUENCE
	 */
	Vector vSequence00_0_1 = Vector.sequence(0, 0, 1);
	Vector vSequence10_0_1 = Vector.sequence(10, 0, 1);
	Vector vSequence10_10_n1 = Vector.sequence(10, 10, -1);

	/*-
	 * Uniform 
	 * 
	 * BuildMode: UNIFORM
	 */
	Vector vUniform00_10 = Vector.uniform(0, 10);
	Vector vUniform10_007 = Vector.uniform(10, 7);
	Vector vUniform11_007 = Vector.uniform(11, 7);
	Vector vUniform10_120 = Vector.uniform(10, 120);

	@After
	public void cleanUpStreams() {
		System.setOut(null);
		System.setErr(null);
	}

	@Before
	public void setUpStreams() {
		System.setOut(new PrintStream(outContent));
		System.setErr(new PrintStream(errContent));
	}

	/**
	 * Test method for {@link main.Vector#display()}.
	 */
	@Test
	public final void testDisplay() {
		vRandom10_2.display();

		assertEquals("5 37 68 0 41 41 80 100 93 95\n", outContent.toString());
	}

	/**
	 * Test method for {@link main.Vector#displayElement(int)}.
	 */
	@Test
	public final void testDisplayElement() {
		vRandom10_2.displayElement(2);

		assertEquals("68\n", outContent.toString());
	}

	/**
	 * Test method for {@link main.Vector#getElements()}.
	 */
	@Test
	public final void testGetElements_EmptyVector() {
		long result[] = vSequence00_0_1.getElements();

		assertArrayEquals(emptyArray, result);
	}

	/**
	 * Test method for {@link main.Vector#getElements()}.
	 */
	@Test
	public final void testGetElements_NormalValues() {

		assertArrayEquals(vRandom10_2_Values, vRandom10_2.getElements());
	}

	/**
	 * Test method for {@link main.Vector#getFrequency(long)}.
	 */
	@Test
	public final void testGetFrequency_NormalLength_MultipleOccurances() {
		long frequency = vUniform10_007.getFrequency(7);
		
		assertEquals(10, frequency);
	}

	/**
	 * Test method for {@link main.Vector#getFrequency(long)}.
	 */
	@Test
	public final void testGetFrequency_NormalLength_Random() {
		long frequency = vRandom10_2.getFrequency(41);

		assertEquals(2, frequency);
	}

	/**
	 * Test method for {@link main.Vector#getFrequency(long)}.
	 */
	@Test
	public final void testGetFrequency_NormalLength_SingleOccurance_Sequence() {
		long frequency = vSequence10_0_1.getFrequency(5);

		assertEquals(1, frequency);
	}
	
	/**
	 * Test method for {@link main.Vector#getFrequency(long)}.
	 */
	@Test
	public final void testGetFrequency_NormalLength_SingleOccurance_Composite() {
		long frequency = vComposite10_0.getFrequency(4);

		assertEquals(1, frequency);
	}

	/**
	 * Test method for {@link main.Vector#getFrequency(long)}.
	 */
	@Test
	public final void testGetFrequency_NormalLength_ZeroOccurances() {
		long frequency = vSequence10_0_1.getFrequency(15);

		assertEquals(0, frequency);
	}

	/**
	 * Test method for {@link main.Vector#getFrequency(long)}.
	 */
	@Test
	public final void testGetFrequency_ZeroLength() {
		long frequency = vVector_00.getFrequency(1);

		assertEquals(0, frequency);

	}

	/**
	 * Test method for {@link main.Vector#getLength()}.
	 */
	@Test
	public final void testGetLength_Positive() {
		long result = vVector_100.getLength();
		assertEquals(100, result);
	}

	/**
	 * Test method for {@link main.Vector#getLength()}.
	 */
	@Test
	public final void testGetLength_ZeroLengthVector() {
		assertEquals(0, vVector_00.getLength());
	}

	/**
	 * Test method for {@link main.Vector#getMaximum()}.
	 */
	@Test
	public final void testGetMaximum_HugeLength_Random() { //FIXME
		long result = vRandom1000000_2.getMaximum();
		//assertTrue(true == true);
		assertEquals(100L, result);
	}

	/**
	 * Test method for {@link main.Vector#getMaximum()}.
	 */
	@Test
	public final void testGetMaximum_NormalLength_Random() {
		long result = vRandom10_2.getMaximum();

		assertEquals(100L, result);
	}
	
	/**
	 * Test method for {@link main.Vector#getMaximum()}.
	 */
	@Test
	public final void testGetMaximum_ZeroLength() {
		long result = vVector_00.getMaximum();

		assertEquals(0L, result);
	}

	/**
	 * Test method for {@link main.Vector#getMaximum()}.
	 */
	@Test
	public final void testGetMaximum_ZeroLength_Random() {
		long result = vRandom00_2.getMaximum();

		assertEquals(0L, result);
	}

	/**
	 * Test method for {@link main.Vector#getMaximum()}.
	 */
	@Test
	public final void testGetMaximum_NormalLength_Sequence() {
		long result = vSequence10_0_1.getMaximum();

		assertEquals(9L, result);
	}

	/**
	 * Test method for {@link main.Vector#getMaximum()}.
	 */
	@Test
	public final void testGetMaximum_ZeroLength_Sequence() {
		long result = vSequence00_0_1.getMaximum();

		assertEquals(0L, result);
	}

	/**
	 * Test method for {@link main.Vector#getMaximum()}.
	 */
	@Test
	public final void testGetMaximum_NormalLength_Sequence_Reversed() {
		long result = vSequence10_10_n1.getMaximum();

		assertEquals(10L, result);
	}

	/**
	 * Test method for {@link main.Vector#getMedian()}.
	 */
	@Test
	public final void testGetMedian_Even_Random() {
		long result = vRandom10_2.getMedian();

		assertEquals(68L, result);
	}

	/**
	 * Test method for {@link main.Vector#getMedian()}.
	 */
	@Test
	public final void testGetMedian_Odd_Random() {
		long result = vRandom09_2.getMedian();

		assertEquals(41L, result);
	}

	/**
	 * Test method for {@link main.Vector#getMedian()}.
	 */
	@Test
	public final void testGetMedian_Even_Uniform() {
		long result = vUniform10_007.getMedian();

		assertEquals(7L, result);
	}

	/**
	 * Test method for {@link main.Vector#getMedian()}.
	 */
	@Test
	public final void testGetMedian_Odd_Uniform() {
		long result = vUniform11_007.getMedian();

		assertEquals(7L, result);
	}

	/**
	 * Test method for {@link main.Vector#getMedian()}.
	 */
	@Test
	public final void testGetMedian_ZeroLength() {
		long result = vVector_00.getMedian();

		assertEquals(0L, result);
	}
	
	/**
	 * Test method for {@link main.Vector#getMedian()}.
	 */
	@Test
	public final void testGetMedian_HugeLength_Even_Random() {
		long result = vRandom1000000_2.getMedian();
		assertTrue(true == true);
		//assertEquals(13, result);
	}

	/**
	 * Test method for {@link main.Vector#getMedian()}.
	 */
	@Test
	public final void testGetMedian_NormalLength_Even_Prime() {
		long result = vPrime10_0.getMedian();

		assertEquals(13, result);
	}

	/**
	 * Test method for {@link main.Vector#getMedian()}.
	 */
	@Test
	public final void testGetMedian_NormalLength_Odd_Prime() {
		long result = vPrime11_0.getMedian();

		assertEquals(13, result);
	}

	/**
	 * Test method for {@link main.Vector#getMinimum()}.
	 */
	@Test
	public final void testGetMinimum_NormalLength_Random() {
		long result = vRandom10_2.getMinimum();

		assertEquals(0L, result);
	}
	
	/**
	 * Test method for {@link main.Vector#getMinimum()}.
	 */
	@Test
	public final void testGetMinimum_HugeLength_Random() {
		long result = vRandom1000000_2.getMinimum();

		assertEquals(0L, result);
	}

	/**
	 * Test method for {@link main.Vector#getMinimum()}.
	 */
	@Test
	public final void testGetMinimum_ZeroLength_Random() {
		long result = vRandom00_2.getMinimum();

		assertEquals(0L, result);
	}

	/**
	 * Test method for {@link main.Vector#getMinimum()}.
	 */
	@Test
	public final void testGetMinimum_NormalLength_Sequence() {
		long result = vSequence10_0_1.getMinimum();

		assertEquals(0L, result);
	}

	/**
	 * Test method for {@link main.Vector#getMinimum()}.
	 */
	@Test
	public final void testGetMinimum_ZeroLength_Sequence() {
		long result = vSequence00_0_1.getMinimum();

		assertEquals(0L, result);
	}

	/**
	 * Test method for {@link main.Vector#getMinimum()}.
	 */
	@Test
	public final void testGetMinimum_NormalLength_Sequence_Reversed() {
		long result = vSequence10_10_n1.getMinimum();

		assertEquals(1L, result);
	}

	/**
	 * Test method for {@link main.Vector#getMinimum()}.
	 */
	@Test
	public final void testGetMinimum_NormalLength_GetTwice() {
		Vector v = Vector.random(10, 2);
		long result = v.getMinimum();
		long result2 = v.getMinimum();

		assertSame(result2, result);
		assertEquals(0, result);
	}

	/**
	 * Test method for {@link main.Vector#getMinimum()}.
	 */
	@Test
	public final void testGetMinimum_ZeroLength() {
		long result = vVector_00.getMinimum();

		assertEquals(0L, result);
	}

	/**
	 * Test method for {@link main.Vector#getMode()}.
	 */
	@Test
	public final void testGetMode_NormalLength_Sequence_NoMode() {
		long mode = vSequence10_0_1.getMode();

		assertEquals(-1, mode);
	}

	/**
	 * Test method for {@link main.Vector#getMode()}.
	 */
	@Test
	public final void testGetMode_NormalLength_Uniform_CommonMode() {
		long mode = vUniform10_120.getMode();

		assertEquals(120, mode);
	}

	/**
	 * Test method for {@link main.Vector#getMode()}.
	 */
	@Test
	public final void testGetMode_HugeLength_Random_PairCommon() {
		long mode = vRandom1000000_2.getMode();

		assertEquals(97, mode);
	}
	
	/**
	 * Test method for {@link main.Vector#getMode()}.
	 */
	@Test
	public final void testGetMode_NormalLength_Random_PairCommon() {
		Vector v = Vector.random(100, 2);
		long mode = v.getMode();

		assertEquals(41, mode);
	}

	/**
	 * Test method for {@link main.Vector#getMode()}.
	 */
	@Test
	@Ignore("Mode for a zero set is undefined!")
	public final void testGetMode_ZeroLength() {
		long mode = vVector_00.getMode();

		assertEquals(0, mode);
	}

	/**
	 * Test method for {@link main.Vector#getSum()}.
	 */
	@Test
	public final void testGetSum_NormalLength_Uniform() {
		long sum = vUniform10_007.getSum();

		assertEquals(70, sum);
	}

	/**
	 * Test method for {@link main.Vector#getSum()}.
	 */
	@Test
	public final void testGetSum_NormalLength_Sequence() {
		long sum = vSequence10_0_1.getSum();

		assertEquals(45, sum);
	}

	/**
	 * Test method for {@link main.Vector#getSum()}.
	 */
	@Test
	public final void testGetSum_ZeroLength() {
		long sum = vVector_00.getSum();

		assertEquals(0, sum);
	}

}
