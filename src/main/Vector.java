package main;

import java.util.*;

public class Vector {

	// ===========================================================================
	// INITIALIZATION
	// ===========================================================================

	private static enum BuildMode {
		ABUNDANT, COMPOSITE, NONE, PQ, PRIME, RANDOM, REVERSED_SORTED, REVERSED, SEQUENCE, SHIFTED, SORTED, SORTED_SAFE, UNIFORM
	}

	private final static ArrayList<BuildMode> sortSafe = new ArrayList<BuildMode>(
			Arrays.asList(BuildMode.ABUNDANT, BuildMode.UNIFORM, BuildMode.SEQUENCE, BuildMode.PQ, BuildMode.PRIME,
					BuildMode.COMPOSITE, BuildMode.SORTED_SAFE, BuildMode.NONE));

	private BuildMode buildMode;
	private boolean sorted = false;
	private long[] sortedElements;

	private Long sum;
	private Long mode;
	private Long median;
	private Long minimum;
	private Long maximum;

	private final int length;
	private final long[] elements;

	/**
	 * Constructs new vector with the given length and all elements set to zero.
	 */
	public Vector(int length) {

		this.buildMode = BuildMode.NONE;
		this.sorted = false;

		this.sum = null;
		this.mode = null;
		this.median = null;
		this.minimum = null;
		this.maximum = null;

		this.length = length;
		this.elements = new long[length];
	}

	/**
	 * Returns new vector with elements generated from the abundant number
	 * sequence starting from the specified value.
	 */
	public static Vector abundant(int length, long start) {
		// FIXME
		Vector vector = new Vector(length);
		int count = 0;
		long i = start;

		while (count < length) {
			if (isAbundant(i)) {
				vector.elements[count] = i;
				count++;
			}
			i++;
		}
		vector.buildMode = BuildMode.ABUNDANT;
		if (vector.length > 0) {
			vector.mode = -1L;
		}
		return vector;
		/*
		 * TODO
		 * 
		 * length 4, start 0 => [12 18 20 24] length 4, start 12 => [12 18 20
		 * 24] length 4, start -1 => [12 18 20 24] length 4, start 42 => [42 48
		 * 54 56]
		 */

		// return new Vector(length);
	}

	/**
	 * Returns new vector with elements generated from the composite number
	 * sequence starting from the specified value.
	 */
	public static Vector composite(int length, long start) {
		// FIXME
		Vector vector = new Vector(length);
		int count = 0;
		long i = start;

		while (count < length) {
			if (isComposite(i)) {
				vector.elements[count] = i;
				count++;
			}
			i++;
		}
		vector.buildMode = BuildMode.COMPOSITE;
		if (vector.length > 0) {
			vector.mode = -1L;
		}
		return vector;
		/*
		 * TODO
		 * 
		 * length 4, start 0 => [4 6 8 9] length 4, start 4 => [4 6 8 9] length
		 * 4, start -1 => [4 6 8 9] length 4, start 42 => [42 44 45 46]
		 */

		// return new Vector(length);
	}

	/**
	 * Returns whether the number is abundant.
	 */
	public static boolean isAbundant(long number) {
		ArrayList<Long> factors = new ArrayList<Long>();

		if (number < 12) {
			return false;
		}

		for (long i = 1; i <= Math.sqrt(number); i++) { // FOR LOOP adds all
														// factors UP TO square
														// root
			if (number % i == 0) {
				factors.add(i);
				if (i != 1 && i != Math.sqrt(number)) {
					factors.add(number / i);
				}
			}
		}

		long total = 0;
		for (int i = 0; i < factors.size(); i++) {
			total += (long) factors.get(i);
		}

		if (total > number) {
			return true;
		}
		// ELSE
		return false;

	}

	/**
	 * Returns whether the number is composite.
	 */
	public static boolean isComposite(long number) {

		if (number < 4) {
			return false;
		}

		ArrayList<Long> factors = new ArrayList<Long>();

		for (long i = 2; i <= Math.sqrt(number); i++) { // FOR LOOP adds all
														// factors UP TO square
														// root
			if (number % i == 0) {
				factors.add(i);
				if (i != 1) {
					factors.add(number / i);
				}
			}
			if (factors.size() >= 2) {
				return true;
			}
		}

		return false;

	}

	/**
	 * Returns whether the number is semiprime.
	 */
	public static boolean isPQ(long number) {

		if (number < 4) {
			return false;
		}

		ArrayList<Long> factors = new ArrayList<Long>();

		for (long i = 2; i <= Math.sqrt(number); i++) { // FOR LOOP adds all
														// factors UP TO square
														// root
			if (number % i == 0) {
				factors.add(i);
				if (i != 1) {
					factors.add(number / i);
				}
			}
			if (factors.size() > 2) {
				return false;
			}
		}

		if (factors.size() == 2) {
			if (Vector.isPrime(factors.get(0)) && Vector.isPrime(factors.get(1))) {
				return true;
			}
			;
		}

		return false;
	}

	/**
	 * Returns whether the number is prime.
	 */
	public static boolean isPrime(long number) {

		if (number < 2 || (number > 2 && number % 2 == 0)) {
			return false;
		}
		// if(number % 2 == 0 && number != 2){ return false;}

		for (long i = 2; i <= Math.sqrt(number); i++) {
			if (number % i == 0) {
				return false;
			}
		}

		return true;

	}

	/**
	 * Returns new vector with elements generated from the pq number sequence
	 * starting from the specified value.
	 */
	public static Vector pq(int length, long start) {
		// FIXME
		Vector vector = new Vector(length);
		int count = 0;
		long i = start;

		while (count < length) {
			if (isPQ(i)) {
				vector.elements[count] = i;
				count++;
			}
			i++;
		}
		vector.buildMode = BuildMode.PQ;
		if (vector.length > 0) {
			vector.mode = -1L;
		}
		return vector;

		/*
		 * TODO
		 * 
		 * length 4, start 1 => [4 6 9 10] length 4, start 4 => [4 6 9 10]
		 * length 4, start -1 => [4 6 9 10] length 4, start 42 => [46 49 51 55]
		 */

		// return new Vector(length);
	}

	/**
	 * Returns new vector with elements generated from the prime number sequence
	 * starting from the specified value.
	 */
	public static Vector prime(int length, long start) {
		// FIXME
		Vector vector = new Vector(length);
		int count = 0;
		long i = start;

		while (count < length) {
			if (isPrime(i)) {
				vector.elements[count] = i;
				count++;
			}
			i++;
		}
		vector.buildMode = BuildMode.PRIME;
		if (vector.length > 0) {
			vector.mode = -1L;
		}
		return vector;
		/*
		 * TODO
		 * 
		 * length 4, start 1 => [2 3 5 7] length 4, start 2 => [2 3 5 7] length
		 * 4, start -1 => [2 3 5 7] length 4, start 42 => [43 47 53 59]
		 */
	}

	public static Vector prime2(int length, long start) {
		Vector vector = new Vector(length);
		// 2^63 MAX LIMIT

		vector.buildMode = BuildMode.PRIME;
		return vector;
	}

	public static long[] prime2_gen(int length, long start) {
		return new Vector(length).elements;
	}

	/**
	 * Returns new vector with elements generated at random up to 100.
	 */
	public static Vector random(int length, long seed) {

		Vector vector = new Vector(length);
		Random random = new Random(seed);

		for (int i = 0; i < length; i++) {
			vector.elements[i] = (long) random.nextInt(101);
		}

		vector.buildMode = BuildMode.RANDOM;
		return vector;
	}
	// ===========================================================================
	// INITIALIZATION
	// ===========================================================================

	/**
	 * Returns new vector with elements in sequence from given start and step.
	 */
	public static Vector sequence(int length, long start, long step) {
		Vector vector = new Vector(length);

		for (int i = 0; i < length; i++) {
			vector.elements[i] = start + (i * step);
		}
		if (step > -1) {
			vector.buildMode = BuildMode.SEQUENCE;
		} else if (step < 0) {
			vector.buildMode = BuildMode.REVERSED_SORTED;
		}
		if (vector.length > 0) {
			vector.mode = -1L;
		}
		return vector;
	}

	/**
	 * Returns new vector with all elements set to given value.
	 */
	public static Vector uniform(int length, long value) {
		Vector vector = new Vector(length);
		Arrays.fill(vector.elements, value);

		vector.buildMode = BuildMode.UNIFORM;
		if (vector.length > 0) {
			vector.sum = value * length;
			vector.mode = value;
			vector.median = value;
			vector.maximum = value;
			vector.minimum = value;
		}
		return vector;

	}

	// ===========================================================================
	// VECTOR OPERATIONS
	// ===========================================================================

	/**
	 * Returns new vector that is a copy of the current vector.
	 */
	public Vector cloned() {
		Vector vector = new Vector(this.length);

		for (int i = 0; i < this.elements.length; i++) {
			vector.elements[i] = this.elements[i];
		}
		vector.buildMode = this.buildMode;
		vector.sum = this.sum;
		vector.mode = this.mode;
		vector.median = this.median;
		vector.minimum = this.maximum;
		vector.maximum = this.minimum;
		return vector;
	}

	/**
	 * Returns the vector elements.
	 */
	public long[] getElements() {

		return this.elements;
	}

	/**
	 * Returns the frequency of the value in the vector.
	 */
	public long getFrequency(long value) {
		long occurances = 0;

		if (this.length == 0) {
			return 0;
		}

		switch (this.buildMode) {
		case UNIFORM:
		case NONE:
			if (this.elements[0] == value) {
				return this.length;
			}
			break;
		case ABUNDANT:
		case COMPOSITE:
		case PQ:
		case PRIME:
		case SEQUENCE:
		case SORTED_SAFE:
		case REVERSED_SORTED:
			for (long element : this.elements) {
				if (element == value) {
					return 1;
				}
			}
			break;
		case SORTED:
		case RANDOM:
		case REVERSED:
			for (long element : this.elements) {
				if (element == value) {
					occurances++;
				}
			}
			return occurances;
		default:
			break;
		}
		return occurances;
	}

	/**
	 * Returns the vector length.
	 */
	public int getLength() {

		return this.length;
	}

	/**
	 * Returns the largest value in the vector.
	 */
	public Long getMaximum() {
		if (this.length == 0) {
			return 0L;
		}
		if (this.maximum != null) {
			return this.maximum;
		}

		switch (this.buildMode) {
		case UNIFORM:
		case NONE:
			this.maximum = this.elements[0];
			return this.maximum;
		case ABUNDANT:
		case COMPOSITE:
		case PQ:
		case PRIME:
		case SEQUENCE:
		case SORTED:
		case SORTED_SAFE:
			this.maximum = this.elements[this.length - 1];
			return this.maximum;
		case REVERSED_SORTED:
			this.maximum = this.elements[0];
			return this.maximum;
		case RANDOM:
			if (!this.sorted) {
				long max = 0;
				for (int i = 0; i < this.length; i++) {
					if (this.elements[i] > max) {
						max = this.elements[i];
					}
					if (max == 100) {
						this.maximum = 100L;
						return this.maximum;
					}
				}
			}
		case REVERSED:
		default:
			if (!this.sorted) {
				this.sort();
			}
			this.maximum = this.sortedElements[this.length - 1];
			return this.maximum;
		}

	}

	/**
	 * Returns the upper median.
	 */
	public Long getMedian() {
		if (this.length == 0) {
			return 0L;
		}
		if (this.median != null) {
			return this.median;
		}

		int elementIndex = 0;

		switch (this.buildMode) {
		case UNIFORM:
		case NONE:
			this.median = this.elements[0];
			return this.median;
		case ABUNDANT:
		case COMPOSITE:
		case PQ:
		case PRIME:
		case SEQUENCE:
		case SORTED:
			if (this.length % 2 == 1) {
				elementIndex = (this.elements.length / 2);
				this.median = this.elements[elementIndex];
			} else {
				elementIndex = (int) Math.ceil((elements.length / 2));
				this.median = this.elements[elementIndex];
			}
			this.median = this.elements[elementIndex];
			return this.median;
		case REVERSED_SORTED:
			if (this.length % 2 == 1) {
				elementIndex = (this.elements.length / 2);
				this.median = this.elements[elementIndex];
			} else {
				elementIndex = (int) Math.ceil((elements.length / 2) - 1);
				this.median = this.elements[elementIndex];
			}
			this.median = this.elements[elementIndex];
			return this.median;
		case RANDOM:
		case REVERSED:
		default:
			if (!this.sorted) {
				this.sort();
			}
			if (this.length % 2 == 1) {
				elementIndex = (this.sortedElements.length / 2);
				this.median = this.sortedElements[elementIndex];
			} else {
				elementIndex = (int) Math.ceil((sortedElements.length / 2));
				this.median = this.sortedElements[elementIndex];
			}
			return this.median;

		}

	}

	/**
	 * Returns the smallest value in the vector.
	 */
	public Long getMinimum() {
		if (this.length == 0) {
			return 0L;
		}
		if (this.minimum != null) {
			return this.minimum;
		}

		switch (this.buildMode) {
		case UNIFORM:
		case NONE:
			return this.elements[0];
		case ABUNDANT:
		case COMPOSITE:
		case PQ:
		case PRIME:
		case SEQUENCE:
		case SORTED_SAFE:
		case SORTED:
			return this.elements[0];
		case RANDOM:
			if (!this.sorted) {
				long min = 100;
				for (int i = 0; i < this.length; i++) {
					if (this.elements[i] < min) {
						min = this.elements[i];
					}
					if (min == 0L) {
						this.minimum = 0L;
						return this.minimum;
					}
				}
			}
		default:
			if (!this.sorted) {
				this.sort();
			}
			this.minimum = this.sortedElements[0];
			return this.minimum;
		}

	}

	// ===========================================================================
	// VECTOR COMPUTATIONS
	// ===========================================================================

	/**
	 * Returns the most frequently occuring element or -1 if there is no such
	 * unique element.
	 */
	public Long getMode() {
		if (this.length == 0) {
			return 0L;
		}
		if (this.mode != null) {
			return this.mode;
		}

		switch (this.buildMode) {
		case UNIFORM:
		case NONE:
			this.mode = this.elements[0];
			return this.elements[0];
		case ABUNDANT:
		case COMPOSITE:
		case PQ:
		case PRIME:
		case SEQUENCE:
		case REVERSED_SORTED:
			this.mode = -1L;
			return -1L;
		case SORTED:
		case RANDOM:
		case REVERSED:
		default:
			HashMap<Long, Integer> freqHashMap = new HashMap<Long, Integer>();
			long maxElement = -1;
			long maxElementCount = 1;

			for (long element : this.elements) {
				if (freqHashMap.containsKey(element)) {
					freqHashMap.put(element, freqHashMap.get(element) + 1);

					if (freqHashMap.get(element) > maxElementCount) {
						maxElement = element;
						maxElementCount = freqHashMap.get(element);
					}
				} else {
					freqHashMap.put(element, 1);
				}
			}
			this.mode = maxElement;
			return this.mode;

		}

	}

	/**
	 * Returns the sum of all elements.
	 */
	public Long getSum() {
		if (this.length == 0) {
			return 0L;
		}
		if (this.sum != null) {
			return this.sum;
		}

		long sum = 0;

		switch (this.buildMode) {
		case UNIFORM:
		case NONE:
			this.sum = this.elements[0] * this.length;
			return this.sum;
		case ABUNDANT:
		case COMPOSITE:
		case PQ:
		case PRIME:
		case SEQUENCE:
		case SORTED:
		case RANDOM:
		default:
			for (long longElement : this.elements) {
				sum = sum + longElement;
			}
			this.sum = sum;
			return sum;
		}

	}

	/**
	 * Returns new vector with elements ordered in reverse.
	 */
	public Vector reversed() {
		Vector vector = new Vector(this.length);

		for (int i = 0; i < this.length; i++) {
			vector.elements[i] = this.elements[this.length - 1 - i];
		}
		vector.buildMode = BuildMode.REVERSED;
		return vector;
	}

	/**
	 * Returns new vector, adding scalar to each element.
	 */
	public Vector scalarAdd(long scalar) {
		Vector vector = new Vector(this.length);

		for (int i = 0; i < this.length; i++) {
			vector.elements[i] = this.elements[i] + scalar;
		}
		vector.buildMode = this.buildMode;
		return vector;
	}

	/**
	 * Returns new vector, multiplying scalar to each element.
	 */
	public Vector scalarMultiply(long scalar) {
		Vector vector = new Vector(this.length);

		for (int i = 0; i < this.length; i++) {
			vector.elements[i] = this.elements[i] * scalar;
		}
		vector.buildMode = this.buildMode;
		return vector;
	}

	/**
	 * Returns new vector with elements shifted right by a given number of
	 * positions.
	 */
	public Vector shifted(int amount) {
		// TODO: Optimize

		int deltaMovement = 0;
		if (Math.abs(amount) > this.elements.length) {
			deltaMovement = Math.abs(amount) % this.elements.length;
		} else {
			deltaMovement = amount;
		}

		long[] shift = new long[this.length];
		int shiftI = 0;

		for (int i = this.elements.length - deltaMovement; i < this.elements.length; i++) {
			shift[shiftI] = this.elements[i];
			shiftI++;
		}

		for (int i = 0; i < (this.elements.length - deltaMovement); i++) {
			shift[shiftI] = this.elements[i];
			shiftI++;
		}

		Vector vector = new Vector(this.length);
		for (int i = 0; i < shift.length; i++) {
			vector.elements[i] = shift[i];
		}
		vector.buildMode = BuildMode.SHIFTED;
		return vector;
	}

	/**
	 * Sorts the current vector.
	 */

	public void sort() {
		if (sortSafe.contains(this.buildMode)) {
			this.sortedElements = elements;
			this.sorted = true;
			return;
		} // No need to sort.
		this.sortedElements = Arrays.copyOf(elements, elements.length);
		Arrays.parallelSort(sortedElements);
		this.sorted = true;

		return;

	}

	/**
	 * Returns new vector with elements ordered from smallest to largest.
	 */
	public Vector sorted() {
		Vector vector = new Vector(this.length);

		if (sortSafe.contains(this.buildMode)) {
			System.arraycopy(this.elements, 0, vector.elements, 0, this.length);
			vector.buildMode = BuildMode.SORTED_SAFE;
		} else if (this.buildMode == BuildMode.REVERSED_SORTED) {
			vector = this.reversed();
			vector.buildMode = BuildMode.SORTED_SAFE;
		} else {
			this.sort();
			for (int i = 0; i < this.sortedElements.length; i++) {
				vector.elements[i] = this.sortedElements[i];
			}
			vector.buildMode = BuildMode.SORTED;
		}

		return vector;
	}

	/**
	 * Returns new vector, adding elements with the same index.
	 */
	public Vector vectorAdd(Vector other) {
		Vector vector = new Vector(this.length);

		for (int i = 0; i < this.length; i++) {
			vector.elements[i] = this.elements[i] + other.elements[i];
		}
		vector.buildMode = this.buildMode;
		return vector;
	}

	/**
	 * Returns new vector, multiplying elements with the same index.
	 */
	public Vector vectorMultiply(Vector other) {
		Vector vector = new Vector(this.length);

		for (int i = 0; i < this.length; i++) {
			vector.elements[i] = this.elements[i] * other.elements[i];
		}
		vector.buildMode = this.buildMode;
		return vector;
	}

	// ===========================================================================
	// DISPLAY OPERATIONS
	// ===========================================================================

	/**
	 * Displays the vector.
	 */
	public void display() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < elements.length; i++) {
			if (i < elements.length - 1) {
				sb.append(elements[i] + " ");
				// out += elements[i] + " ";
				// System.out.print(elements[i] + " ");
			} else {
				sb.append(elements[i] + "\n");
				// out += elements[i] + "\n";
				// System.out.print(elements[i] + "\n");
			}

		}
		System.out.print(sb.toString());
	}

	/**
	 * Displays the element at the specified index.
	 */
	public void displayElement(int index) {
		System.out.print(this.elements[index] + "\n");

	}
}
